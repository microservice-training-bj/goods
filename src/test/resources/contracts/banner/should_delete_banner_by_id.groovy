package banner

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'DELETE'
        url '/api/banners/1'
        headers {
            header('Content-Type', 'application/json')
        }

    }
    response {
        status 202
    }
}
