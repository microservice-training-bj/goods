package banner

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/api/banners'
        body("""
        {
          "name":"createdBanners",
          "goodsId":"1"
        }
        """)
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 201
    }
}
