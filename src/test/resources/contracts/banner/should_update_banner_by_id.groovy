package banner

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'PUT'
        url '/api/banners/2'
        headers {
            header('Content-Type', 'application/json')
        }
        body("""
        {
          "goodsId":"2"
        }
        """)
    }
    response {
        status 202
        body("""
            {"id":"2","goodsId":"2"}
        """)
    }
}
