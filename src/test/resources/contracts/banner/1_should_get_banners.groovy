package banner

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/banners'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 200
        body("""
            [[{"id":"1","goodsId":"1"}]]
        """)
    }
}
