package goods

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'PUT'
        url '/api/goods/2'
        headers {
            header('Content-Type', 'application/json')
        }
        body("""
        {
          "name":"updatedGoods",
          "productId":"2"
        }
        """)
    }
    response {
        status 202
        body("""
            {"id":"2","name":"updatedGoods","productId":"2"}
        """)
    }
}
