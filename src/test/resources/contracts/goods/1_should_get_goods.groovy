package goods

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/goods'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 200
        body("""
            [[{"id":"1","name":"firstGoods","productId":"1"},{"id":"2","name":"secondGoods","productId":"1"}]]
        """)
    }
}
