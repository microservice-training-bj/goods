package goods

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/api/goods'
        body("""
        {
          "name":"createdGoods",
          "productId":"1"
        }
        """)
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 201
    }
}
