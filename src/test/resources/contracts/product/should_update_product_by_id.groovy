package product

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'PUT'
        url '/api/products/2'
        headers {
            header('Content-Type', 'application/json')
        }
        body("""
        {
          "name":"updatedProduct"
        }
        """)
    }
    response {
        status 202
        body("""
            {"id":"2","name":"updatedProduct"}
        """)
    }
}
