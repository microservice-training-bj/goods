package product

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'POST'
        url '/api/products'
        body("""
        {
          "name":"createdProduct"
        }
        """)
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 201
    }
}
