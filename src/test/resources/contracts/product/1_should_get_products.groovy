package product

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    request {
        method 'GET'
        url '/api/products'
        headers {
            header('Content-Type', 'application/json')
        }
    }
    response {
        status 200
        body("""
            [[{"id":"1","name":"firstProduct"}], [{"id":"2","name":"secondProduct"}]]
        """)
    }
}
