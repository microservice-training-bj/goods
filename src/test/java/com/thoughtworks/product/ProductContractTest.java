package com.thoughtworks.product;

import com.thoughtworks.ContractBaseTest;
import com.thoughtworks.product.entity.Product;
import org.junit.Before;


public class ProductContractTest extends ContractBaseTest {

    @Before
    public void setup() {
        productRepository.deleteAll();
        buildProduct("1", "firstProduct");
        buildProduct("2", "secondProduct");
    }

    private void buildProduct(String id, String name) {
        Product product = new Product();
        product.setId(id);
        product.setName(name);
        productRepository.save(product);
    }
}
