package com.thoughtworks;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import com.thoughtworks.banner.repository.BannerRepository;
import com.thoughtworks.goods.repository.GoodsRepository;
import com.thoughtworks.product.repository.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = GoodsApplication.class)
public class ContractBaseTest {
    @Autowired
    private WebApplicationContext context;
    @Autowired
    protected GoodsRepository goodsRepository;
    @Autowired
    protected ProductRepository productRepository;
    @Autowired
    protected BannerRepository bannerRepository;

    @Before
    public void setup() {
        RestAssuredMockMvc.webAppContextSetup(context);
    }

    @Test
    public void contextLoads() {
    }
}
