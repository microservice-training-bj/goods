package com.thoughtworks.banner;

import com.thoughtworks.ContractBaseTest;
import com.thoughtworks.banner.entity.Banner;
import org.junit.Before;


public class BannersContractTest extends ContractBaseTest {
    @Before
    public void setup() {
        bannerRepository.deleteAll();

        buildBanner("1", "1");
        buildBanner("2", "1");
    }

    private void buildBanner(String id, String goodsId) {
        Banner banner = new Banner();
        banner.setId(id);
        banner.setGoodsId(goodsId);

        bannerRepository.save(banner);
    }

}
