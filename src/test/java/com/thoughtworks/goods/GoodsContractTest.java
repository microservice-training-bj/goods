package com.thoughtworks.goods;

import com.thoughtworks.ContractBaseTest;
import com.thoughtworks.goods.entity.Goods;
import org.junit.Before;


public class GoodsContractTest extends ContractBaseTest {
    @Before
    public void setup() {
        goodsRepository.deleteAll();
        buildGoods("1", "1", "firstGoods");
        buildGoods("2", "1", "secondGoods");
    }

    private void buildGoods(String id, String productId, String name) {
        Goods goods = new Goods();
        goods.setId(id);
        goods.setProductId(productId);
        goods.setName(name);
        goodsRepository.save(goods);
    }
}
