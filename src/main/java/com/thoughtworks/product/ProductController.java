package com.thoughtworks.product;

import com.thoughtworks.product.repository.ProductRepository;
import com.thoughtworks.product.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Product> getProducts() {
        return productRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createProduct(@RequestBody Product product) {
        product.setId(UUID.randomUUID().toString());
        productRepository.save(product);
    }

    @PutMapping(value = "/{productId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Product updateProduct(@PathVariable String productId,
                                 @RequestBody Product product) {
        Product existProduct = productRepository.findById(productId);
        if (null == existProduct) {
           throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
        }
        existProduct.setName(product.getName());
        return productRepository.saveAndFlush(existProduct);
    }
}
