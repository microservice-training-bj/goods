package com.thoughtworks.goods.api;

import com.thoughtworks.goods.repository.GoodsRepository;
import com.thoughtworks.goods.entity.Goods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/goods")
public class GoodsController {

    @Autowired
    private GoodsRepository goodsRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Goods> getGoods() {
        return goodsRepository.findAll();
    }

    @GetMapping(value = "/{goodsId}")
    @ResponseStatus(HttpStatus.OK)
    public Goods getGoods(@PathVariable Integer goodsId) {
        return goodsRepository.findOne(goodsId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createGoods(@RequestBody Goods goods) {
        goods.setId(UUID.randomUUID().toString());
        goodsRepository.save(goods);
    }

    @PutMapping(value = "/{goodsId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Goods updateGoods(@PathVariable String goodsId,
                               @RequestBody Goods goods) {
        Goods existGoods = goodsRepository.findById(goodsId);
        if (null == existGoods) {
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
        }
        existGoods.setName(goods.getName());
        existGoods.setProductId(goods.getProductId());
        return goodsRepository.saveAndFlush(existGoods);
    }
}
