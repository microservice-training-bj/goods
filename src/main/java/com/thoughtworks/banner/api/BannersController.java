package com.thoughtworks.banner.api;

import com.thoughtworks.banner.entity.Banner;
import com.thoughtworks.banner.repository.BannerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = "/api/banners")
public class BannersController {

    @Autowired
    private BannerRepository bannerRepository;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<Banner> list() {
        return bannerRepository.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@RequestBody Banner banner) {
        banner.setId(UUID.randomUUID().toString());
        bannerRepository.save(banner);
    }

    @PutMapping(value = "/{bannerId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Banner update(@PathVariable(value = "bannerId") String bannerId, @RequestBody Banner newBanner) throws HttpStatusCodeException {

        Banner banner = bannerRepository.findById(bannerId);

        if (banner == null) {
            throw new HttpServerErrorException(HttpStatus.NOT_FOUND);
        }

        banner.setGoodsId(newBanner.getGoodsId());
        return bannerRepository.saveAndFlush(banner);
    }

    @DeleteMapping(value = "/{bannerId}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void delete(@PathVariable(value = "bannerId") String bannerId) {
        bannerRepository.deleteById(bannerId);
    }
}
