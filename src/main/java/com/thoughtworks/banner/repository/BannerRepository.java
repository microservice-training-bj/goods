package com.thoughtworks.banner.repository;

import com.thoughtworks.banner.entity.Banner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface BannerRepository extends JpaRepository<Banner, Integer> {

    Banner findById(String bannerId);

    @Transactional
    void deleteById(String bannerId);
}
