CREATE TABLE `product` (
  `id`   VARCHAR(255) NOT NULL PRIMARY KEY,
  `name` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `goods` (
  `id`         VARCHAR(255) NOT NULL PRIMARY KEY,
  `product_id` VARCHAR(255) DEFAULT NULL,
  `name`       VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `banner` (
  `id`       VARCHAR(255) NOT NULL PRIMARY KEY,
  `sequence` INT(11)      DEFAULT NULL,
  `goods_id` VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
);
