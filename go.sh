#! /usr/bin/env bash

STUB_API_NAME=$(basename ${PWD})-api-stub
PRIVATE_REPO=10.202.129.124:5000
ENV=$2

PROD_REPO=xxxx:5000
UAT_REPO=xxxx:5000

function build-services {
#    sed -i "s#%NAME%#$APPLICATION_NAME#g" service.json
#    sed -i "s#%ENV%#$ENV#g" service.json
#    docker run --rm -v $(pwd):/opt/app -v /etc/hosts:/etc/hosts -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 ./gradlew clean build --refresh-dependencies -x test -x pmdMain -x pmdTest -x check -Penv=$ENV
    docker login -u admin -p admin123 $PRIVATE_REPO
    docker build -t goods/goods:$GO_PIPELINE_COUNTER .
    docker tag goods/goods:$GO_PIPELINE_COUNTER ${PRIVATE_REPO}/goods/goods:$GO_PIPELINE_COUNTER
    docker push ${PRIVATE_REPO}/goods/goods:$GO_PIPELINE_COUNTER
    docker rmi ${PRIVATE_REPO}/goods/goods:$GO_PIPELINE_COUNTER
}

function build4uat {
    docker login -u admin -p admin123 -e zzzhao@thoughtworks.com nexusotr.chinacloudapp.cn:5000
    docker run --rm -v $(pwd):/opt/app -v /etc/hosts:/etc/hosts -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 ./gradlew clean build -x test -x pmdMain -x pmdTest -x check -Penv=$ENV
    cp uatproddockerfile Dockerfile
    sed -i 's/prod1/prod2/g' Dockerfile
    sed -i 's#java#java -javaagent:/opt/appdynamics/application/ver4.3.3.2/javaagent.jar#g' supervisor.conf
    sed -i 's/APPDYNAMICSIP/172.21.2.138/g' controller-info.xml
    sed -i '/update-ca-certificates/a\COPY controller-info.xml /opt/appdynamics/application/ver4.3.3.2/conf/' Dockerfile
    sed -i 's/172.21.0/172.21.2/g' filebeat.yml
    docker build -t $UAT_REPO/otr/vehicle-management:$ENV$GO_PIPELINE_COUNTER .
    docker push $UAT_REPO/otr/vehicle-management:$ENV$GO_PIPELINE_COUNTER
    docker rmi $UAT_REPO/otr/vehicle-management:$ENV$GO_PIPELINE_COUNTER
}

function build-prod {
    docker login -u admin -p admin123 -e zzzhao@thoughtworks.com nexusotr.chinacloudapp.cn:5000
    docker login -u admin -p admin123 -e zzzhao@thoughtworks.com $PROD_REPO
    docker run --rm -v $(pwd):/opt/app -v /etc/hosts:/etc/hosts -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 ./gradlew build -x test -x pmdMain -x pmdTest -Penv=$ENV
    cp uatproddockerfile Dockerfile
    sed -i 's/prod1/prod2/g' Dockerfile
    sed -i 's#java#java -javaagent:/opt/appdynamics/application/ver4.3.3.2/javaagent.jar#g' supervisor.conf
    sed -i 's/APPDYNAMICSIP/172.21.0.138/g' controller-info.xml
    sed -i '/update-ca-certificates/a\COPY controller-info.xml /opt/appdynamics/application/ver4.3.3.2/conf/' Dockerfile
    docker build -t $PROD_REPO/otr/vehicle-management:$ENV$GO_PIPELINE_COUNTER .
    docker push $PROD_REPO/otr/vehicle-management:$ENV$GO_PIPELINE_COUNTER
    docker tag $PROD_REPO/otr/vehicle-management:$ENV$GO_PIPELINE_COUNTER $PROD_REPO/otr/vehicle-management:$ENV
    docker push $PROD_REPO/otr/vehicle-management:$ENV
    docker rmi $PROD_REPO/otr/vehicle-management:$ENV$GO_PIPELINE_COUNTER $PROD_REPO/otr/vehicle-management:$ENV
}

function test-project {
    docker run --rm -v $(pwd):/opt/app -v /etc/hosts:/etc/hosts -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 /bin/bash -c "ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime; ./gradlew clean test -Penv=test"
}

function test-coverage {
    docker run --rm -v $(pwd):/opt/app -v /etc/hosts:/etc/hosts -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 ./gradlew junitPlatformJacocoReport -Penv=test
    docker run --rm -v $(pwd):/opt/app -v /etc/hosts:/etc/hosts -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 ./gradlew junitPlatformJacocoCoverageReport -Penv=test
}

function deploy {
    rancher-compose -p ${GO_PIPELINE_NAME} up -d -c --upgrade
}

function run-api-stub {
    docker run -d --name ${STUB_API_NAME} -v ${PWD}/apis-stub:/apis -w /apis -p 3001:3000 ${PRIVATE_REPO}/xkli/raml-server:latest node /raml/ramlServer.js
}

function kill-api-stub {
    docker kill ${STUB_API_NAME}
    docker rm ${STUB_API_NAME}
}


function display-usage {
    echo 'Usage: $ bash go.sh { build-services | test-project | run-api-stub | kill-api-stub | run/kill-mysql}'
}

function run-mysql {
    kill-mysql
    docker run  -d --name test-mysql -e MYSQL_ROOT_PASSWORD=dev -e MYSQL_DATABASE=otr_vehicle -p 3306:3306 mysql:5.7 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
}

function kill-mysql {
    docker kill test-mysql
    docker rm test-mysql
}

function pmd-check {
    docker run --rm -v $(pwd):/opt/app  -w /opt/app ${PRIVATE_REPO}/otr/gradle:jdk8 ./gradlew clean pmdMain pmdTest check -x test
}


function run-account {
    docker run --name account-management-mysql -d -v /tmp/test-mysql:/var/lib/mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=dev -e MYSQL_DATABASE=otr_account mysql:5.7 --character-set-server=utf8mb4 --collation-server=utf8mb4_unicode_ci
    docker run --name account-management-redis -d redis:3 redis-server --appendonly yes
    docker pull ${PRIVATE_REPO}/otr/account-management:latest

    echo 'waiting for mysql ready...'
    while [ 1 == 1 ]
    do
        docker logs account-management-mysql 2>&1 |grep "'/var/run/mysqld/mysqld.sock'  port: 3306"
        if [[ $? == 0 ]];then
            echo 'mysql is ready'
            break
        else
            echo "mysql not ready"
            sleep 1
        fi
    done

    echo 'start to run account-management'
    docker run --name account-management -d -e DB_USER=root -e USER_PASSWD=dev --link account-management-redis:redis --link account-management-mysql:mysql -p 30000:8080 ${PRIVATE_REPO}/otr/account-management:latest
}

function kill-account {
    docker kill account-management
    docker rm account-management
    docker kill account-management-mysql
    docker rm account-management-mysql
    docker kill account-management-redis
    docker rm account-management-redis
}


case $1 in
    build-services)
        build-services
        ;;
    deploy)
        deploy
        ;;
    build-uat)
        build4uat
        ;;
    build-prod)
        build-prod
        ;;
    test-project)
        test-project
        ;;
    run-api-stub)
        run-api-stub
        ;;
    kill-api-stub)
        kill-api-stub
        ;;
    localenv-start)
        run-mysql
        ;;
    localenv-stop)
        kill-mysql
    ;;
    run-mysql)
        run-mysql
        ;;
    kill-mysql)
        kill-mysql
        ;;
    pmd-check)
        pmd-check
        ;;
    run-account)
        run-account
        ;;
    kill-account)
        kill-account
        ;;
    test-coverage)
        test-coverage
        ;;
    *)
        display-usage
        exit -1
esac
