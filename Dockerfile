FROM        java:alpine
EXPOSE      8080
COPY        build/libs/goods-*.jar  /goods/
WORKDIR     /goods
CMD java -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC -verbose:gc -XX:+PrintTenuringDistribution -XX:+PrintGCApplicationStoppedTime -Xloggc:gc_$(date +%Y%m%d-%H%M%S).log -jar goods-0.0.1-SNAPSHOT.jar
